const express = require('express');
const router = new express.Router();

const {
  register,
  login,
  forgotPassword,
} = require('../controllers/authController.js');

const {asyncWrapper} = require('../utils/helpers.js');

router.post('/register', asyncWrapper(register));
router.post('/login', asyncWrapper(login));
router.post('/forgot_password', asyncWrapper(forgotPassword));

module.exports = router;
