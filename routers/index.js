module.exports = (app) => {
  app.use('/api/auth', require('./authRouter.js'));
  app.use('/api/users', require('./userRouter.js'));
  app.use('/api/trucks', require('./truckRouter.js'));
  app.use('/api/loads', require('./loadRouter.js'));
};
