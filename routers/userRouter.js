const express = require('express');
const router = new express.Router();

const {isAuthorized} = require('../middlewares/authMiddleware.js');

const {
  getProfileInfo,
  deleteProfile,
  changeProfilePassword,
} = require('../controllers/userController.js');

const {asyncWrapper} = require('../utils/helpers.js');

router.get('/me', isAuthorized, getProfileInfo);
router.delete('/me', isAuthorized, asyncWrapper(deleteProfile));
router.patch('/me/password', isAuthorized, asyncWrapper(changeProfilePassword));

module.exports = router;
