const express = require('express');
const router = new express.Router();

const {roles} = require('../models/User.js');
const {isAuthorized} = require('../middlewares/authMiddleware.js');

const {
  getUserTrucks,
  addTruckForUser,
  getUserTruckById,
  updateUserTruckById,
  deleteUserTruckById,
  assignTruckToUserById,
} = require('../controllers/truckController.js');

const {asyncWrapper} = require('../utils/helpers.js');

router.get('/', isAuthorized(roles.DRIVER), asyncWrapper(getUserTrucks));
router.post('/', isAuthorized(roles.DRIVER), asyncWrapper(addTruckForUser));
router.get('/:id', isAuthorized(roles.DRIVER), asyncWrapper(getUserTruckById));
router.put('/:id', isAuthorized(roles.DRIVER), asyncWrapper(updateUserTruckById));
router.delete('/:id', isAuthorized(roles.DRIVER), asyncWrapper(deleteUserTruckById));
router.post('/:id/assign', isAuthorized(roles.DRIVER), asyncWrapper(assignTruckToUserById));

module.exports = router;
