const express = require('express');
const router = new express.Router();

const {roles} = require('../models/User.js');
const {isAuthorized} = require('../middlewares/authMiddleware.js');

const {
  getUserLoads,
  addLoadForUser,
  getUserActiveLoad,
  iterateToNextLoadState,
  getUserLoadById,
  updateUserLoadById,
  deleteUserLoadById,
  postUserLoadById,
  getUserLoadShippingInfo,
} = require('../controllers/loadController.js');

const {asyncWrapper} = require('../utils/helpers.js');

router.get('/', isAuthorized(roles.SHIPPER), asyncWrapper(getUserLoads));
router.post('/', isAuthorized(roles.SHIPPER), asyncWrapper(addLoadForUser));
router.get('/active', isAuthorized(roles.DRIVER), asyncWrapper(getUserActiveLoad));
router.patch('/active/state', isAuthorized(roles.DRIVER), asyncWrapper(iterateToNextLoadState));
router.get('/:id', isAuthorized(), asyncWrapper(getUserLoadById));
router.put('/:id', isAuthorized(roles.SHIPPER), asyncWrapper(updateUserLoadById));
router.delete('/:id', isAuthorized(roles.SHIPPER), asyncWrapper(deleteUserLoadById));
router.post('/:id/post', isAuthorized(roles.SHIPPER), asyncWrapper(postUserLoadById));
router.get('/:id/shipping_info', isAuthorized(roles.SHIPPER), asyncWrapper(getUserLoadShippingInfo));

module.exports = router;
