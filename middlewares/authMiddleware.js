const jwt = require('jsonwebtoken');
const {JWT_SECRET} = require('../config');
const {BadRequestError, UnauthorizedError, AccessDeniedError} = require('../utils/errors');

const isAuthorized = (role) => {
  return (req, res, next) => {
    const header = req.headers['authorization'];

    if (!header) {
      throw new UnauthorizedError(`No Authorization http header found!`);
    }

    let [tokenType, token] = header.split(' ');

    if (!token && !tokenType) {
      throw new UnauthorizedError(`No JWT token found!`);
    }

    if (!token) {
      // crutches are our everything
      token = tokenType;
    }

    try {
      const user = jwt.verify(token, JWT_SECRET);

      req.user = user;
    } catch (e) {
      throw new BadRequestError('Invalid token');
    }

    if (role && req.user.role !== role) {
      throw new AccessDeniedError(`Only ${role} can do that staff`);
    }

    next();
  };
};

module.exports = {
  isAuthorized,
};
