/** * @class Represents Bad Request Error*/
class BadRequestError extends Error {
  /** * @param {string} message The message to be sent in json. */
  constructor(message = 'Bad request') {
    super(message);
    this.statusCode = 400;
  }
}

/** * @class Represents Unauthorized Error*/
class UnauthorizedError extends Error {
  /** * @param {string} message The message to be sent in json.*/
  constructor(message = 'Unauthorized user!') {
    super(message);
    this.statusCode = 401;
  }
}

/** * @class Represents Access Denied Error*/
class AccessDeniedError extends Error {
  /** * @param {string} message The message to be sent in json.*/
  constructor(message = 'Access Denied') {
    super(message);
    this.statusCode = 403;
  }
}

module.exports = {
  BadRequestError,
  UnauthorizedError,
  AccessDeniedError,
};
