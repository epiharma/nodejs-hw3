const {Load, validateLoad, statuses} = require('../models/Load.js');
const {Truck} = require('../models/Truck.js');
const {BadRequestError} = require('../utils/errors');

const getUserLoads = async (req, res) => {
  const {user} = req;
  let {limit, offset, status} = req.query;

  offset = Number(offset);
  limit = Number(limit);

  let loads = await Load.find({created_by: user._id}).skip(offset).limit(limit);

  if (status) {
    loads = loads.filter((load) => load.status === status);
  }

  return res.status(200).json({loads});
};

const addLoadForUser = async (req, res) => {
  const {user} = req;
  const {
    name,
    payload,
    pickup_address,
    delivery_address,
    dimensions,
  } = req.body;

  const validationResult = validateLoad({
    name,
    payload,
    pickup_address,
    delivery_address,
    dimensions,
  });

  if (validationResult.error) {
    throw new BadRequestError(validationResult.error.message);
  }

  const load = new Load({
    created_by: user._id,
    name,
    payload,
    pickup_address,
    delivery_address,
    dimensions,
  });

  await load.save();

  return res.status(200).json({message: 'Success'});
};

const getUserActiveLoad = async (req, res) => {
  const {user} = req;


  const truck = await Truck.findOne({created_by: user._id, assigned_to: user._id});
  const load = await Load.findOne({assigned_to: truck._id});

  if (!load) {
    throw new BadRequestError('You don\'t have any active loads');
  }

  return res.status(200).json({load});
};

const iterateToNextLoadState = async (req, res) => {
  const {user} = req;

  const truck = await Truck.findOne({
    created_by: user._id,
    assigned_to: user._id,
  });
  const load = await Load.findOne({assigned_to: truck._id});

  if (!load) {
    throw new BadRequestError('You don\'t have any active loads');
  }

  const state = load.iterateToNextState();
  await load.save();

  return res.status(200).json({message: `Load state changed to ${state}`});
};

const getUserLoadById = async (req, res) => {
  const {user} = req;
  const {id} = req.params;

  const load = await Load.findById(id).exec();

  if (!load) {
    throw new BadRequestError('Load is not found');
  }

  if (!load.isUserLoad(user)) {
    throw new BadRequestError('You can\`t touch anothers loads');
  }

  return res.status(200).json({load});
};

const updateUserLoadById = async (req, res) => {
  const {user} = req;
  const {id} = req.params;
  const {
    name,
    payload,
    pickup_address,
    delivery_address,
    dimensions,
  } = req.body;

  const validationResult = validateLoad({
    name,
    payload,
    pickup_address,
    delivery_address,
    dimensions,
  });

  if (validationResult.error) {
    throw new BadRequestError(validationResult.error.message);
  }

  const load = await Load.findById(id).exec();

  if (!load) {
    throw new BadRequestError('Load is not found');
  }

  if (!load.isUserLoad(user)) {
    throw new BadRequestError('You can\`t touch anothers loads');
  }

  await load.update({
    name,
    payload,
    pickup_address,
    delivery_address,
    dimensions,
  });

  return res.status(200).json({message: 'Success'});
};

const deleteUserLoadById = async (req, res) => {
  const {user} = req;
  const {id} = req.params;

  const load = await Load.findById(id).exec();

  if (!load) {
    throw new BadRequestError('Load is not found');
  }

  if (!load.isUserLoad(user)) {
    throw new BadRequestError('You can\`t touch anothers loads');
  }

  if (load.isInProgress()) {
    throw new BadRequestError(`You can\'t delete load in progress`);
  }

  await load.remove();

  return res.status(200).json({message: 'Success'});
};

const postUserLoadById = async (req, res) => {
  const {user} = req;
  const {id} = req.params;

  const load = await Load.findById(id).exec();

  if (!load) {
    throw new BadRequestError('Load is not found');
  }

  if (!load.isUserLoad(user)) {
    throw new BadRequestError('You can\`t touch anothers loads');
  }

  load.setStatus(statuses.POSTED);
  await load.save();

  const trucks = await Truck.find();
  const suitableTruck = trucks.find((truck) => truck.assigned_to !== null);

  if (suitableTruck) {
    load.assignTo(suitableTruck);
    await load.save();

    suitableTruck.setStatus('OL');

    return res.status(200).json({message: 'Load posted successfully', driver_found: true});
  }

  return res.status(200).json({message: 'No appropriate truck', driver_found: false});
};

const getUserLoadShippingInfo = async (req, res) => {
  const {user} = req;
  const {id} = req.params;

  const load = await Load.findById(id).exec();

  if (!load) {
    throw new BadRequestError('Load is not found');
  }

  if (!load.isUserLoad(user)) {
    throw new BadRequestError('You can\`t touch anothers loads');
  }

  const truck = await Truck.findById(load.assigned_to).exec();

  return res.status(200).json({load, truck});
};

module.exports = {
  getUserLoads,
  addLoadForUser,
  getUserActiveLoad,
  iterateToNextLoadState,
  getUserLoadById,
  updateUserLoadById,
  deleteUserLoadById,
  postUserLoadById,
  getUserLoadShippingInfo,
};
