const {Truck} = require('../models/Truck.js');
const {BadRequestError} = require('../utils/errors');

const getUserTrucks = async (req, res) => {
  const {user} = req;
  let {limit, offset} = req.query;

  offset = Number(offset);
  limit = Number(limit);

  const trucks = await Truck.find({created_by: user._id})
      .skip(offset).limit(limit);

  return res.status(200).json({trucks});
};

const addTruckForUser = async (req, res) => {
  const {user} = req;
  const {type} = req.body;

  const truck = new Truck({
    created_by: user._id,
    type,
  });

  await truck.save();

  return res.status(200).json({message: 'Truck created successfully'});
};

const getUserTruckById = async (req, res) => {
  const {user} = req;
  const {id} = req.params;

  const truck = await Truck.findById(id).exec();

  if (!truck) {
    throw new BadRequestError('Truck is not found');
  }

  if (!truck.isUserTruck(user)) {
    throw new BadRequestError('You can\'t touch anothers trucks');
  }

  return res.status(200).json({truck});
};

const updateUserTruckById = async (req, res) => {
  const {user} = req;
  const {id} = req.params;
  const {type} = req.body;

  const truck = await Truck.findById(id).exec();

  if (!truck) {
    throw new BadRequestError('Truck is not found');
  }

  if (!truck.isUserTruck(user)) {
    throw new BadRequestError('You can\'t touch anothers trucks');
  }

  if (truck.isAssigned()) {
    throw new BadRequestError('You can\'t change your wheels on the road');
  }

  truck.type = type;
  await truck.save();

  return res.status(200).json({message: 'Success'});
};

const deleteUserTruckById = async (req, res) => {
  const {user} = req;
  const {id} = req.params;

  const truck = await Truck.findById(id).exec();

  if (!truck) {
    throw new BadRequestError('Truck is not found');
  }

  if (!truck.isUserTruck(user)) {
    throw new BadRequestError('You can\'t touch anothers trucks');
  }

  await truck.remove();

  return res.status(200).json({message: 'Success'});
};

const assignTruckToUserById = async (req, res) => {
  const {user} = req;
  const {id} = req.params;

  const truck = await Truck.findById(id).exec();

  if (!truck) {
    throw new BadRequestError('Truck is not found');
  }

  if (!truck.isUserTruck(user)) {
    throw new BadRequestError('You can\'t touch anothers trucks');
  }

  truck.assigned_to = req.user._id;
  await truck.save();

  return res.status(200).json({message: 'Truck assigned successfully'});
};

module.exports = {
  getUserTrucks,
  addTruckForUser,
  getUserTruckById,
  updateUserTruckById,
  deleteUserTruckById,
  assignTruckToUserById,
};
