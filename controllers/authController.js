const {User, validateUser} = require('../models/User.js');
const {BadRequestError} = require('../utils/errors');

const register = async (req, res) => {
  const {email, password, role} = req.body;
  const validationResult = validateUser({email, password, role});

  if (validationResult.error) {
    throw new BadRequestError(validationResult.error.message);
  }

  let user = await User.findOne({email});

  if (user) {
    throw new BadRequestError('This email is used');
  }

  user = new User({email, password, role});
  await user.save();

  return res.status(200).json({message: 'Success'});
};

const login = async (req, res) => {
  const {email, password} = req.body;

  if (!email || !password) {
    throw new BadRequestError('Enter your credentials');
  }

  const user = await User.findOne({email});

  if (!user) {
    throw new BadRequestError(`No user with that email found`);
  }

  if (!user.checkPassword(password)) {
    throw new BadRequestError(`Wrong password`);
  }

  const token = user.generateAuthToken();

  return res.status(200).json({jwt_token: token});
};

const forgotPassword = async (req, res) => {
  const {email} = req.body;


  if (!email) {
    throw new BadRequestError('Enter your email address');
  }

  const user = await User.findOne({email});

  if (!user) {
    throw new BadRequestError(`No user with that email found`);
  }

  return res.status(200).json('New password sent to your email address');
};

module.exports = {
  register,
  login,
  forgotPassword,
};
