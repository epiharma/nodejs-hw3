const {User} = require('../models/User');
const {BadRequestError} = require('../utils/errors');

const getProfileInfo = (req, res) => {
  const {user} = req;

  return res.status(200).json({_id: user._id, email: user.email, created_date: user.created_date});
};

const deleteProfile = async (req, res) => {
  const user = await User.findById(req.user._id);

  await user.remove();

  return res.status(200).json({message: 'Success'});
};

const changeProfilePassword = async (req, res) => {
  const {oldPassword, newPassword} = req.body;

  if (!oldPassword || !newPassword) {
    throw new BadRequestError('Password can\'t be empty');
  }

  const user = await User.findById(req.user._id);
  const passwordsMatched = await user.checkPassword(oldPassword);

  if (!passwordsMatched) {
    throw new BadRequestError('Wrong old password');
  }

  user.password = newPassword;
  await user.save();

  return res.status(200).json({message: 'Success'});
};

module.exports = {
  getProfileInfo,
  deleteProfile,
  changeProfilePassword,
};
