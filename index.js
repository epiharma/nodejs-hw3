const express = require('express');
const morgan = require('morgan');
const mongoose = require('mongoose');

const app = express();

const {
  PORT,
  MONGO_URI,
} = require('./config');

const {errorHandler} = require('./utils/helpers');

app.use(express.urlencoded({extended: false}));
app.use(express.json());
app.use(morgan('tiny'));

require('./routers/index.js')(app);

app.use(errorHandler);

(async () => {
  await mongoose
      .connect(MONGO_URI,
          {
            useNewUrlParser: true,
            useUnifiedTopology: true,
            useFindAndModify: false,
            useCreateIndex: true,
          },
      )
      .then(() => {
        console.log('Mongo connected');
      })
      .catch((e) => {
        console.log(e.message);
      });

  app.listen(PORT, () => {
    console.log(`Server works at port ${PORT}!`);
  });
})();
