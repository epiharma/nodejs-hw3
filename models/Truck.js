const mongoose = require('mongoose');

const types = {
  SPRINTER: {
    name: 'SPRINTER',
    length: 300,
    width: 250,
    height: 170,
    weight: 1700,
  },
  SMALL_STRAIGHT: {
    name: 'SMALL STRAIGHT',
    length: 500,
    width: 250,
    height: 170,
    weight: 2500,
  },
  LARGE_STRAIGHT: {
    name: 'LARGE STRAIGHT',
    length: 700,
    width: 350,
    height: 200,
    weight: 4000,
  },
};

const statuses = {
  IS: 'IS',
  OL: 'OL',
};

const TruckSchema = new mongoose.Schema(
    {
      created_by: {
        type: mongoose.Schema.Types.ObjectId,
        required: true,
        ref: 'users',
      },
      assigned_to: {
        type: mongoose.Schema.Types.ObjectId,
        default: null,
        ref: 'users',
      },
      type: {
        type: String,
        required: true,
      },
      status: {
        type: String,
        default: statuses.IS,
      },
      created_date: {
        type: Date,
        default: Date.now(),
      },
    },
    {
      collection: 'trucks',
      versionKey: false,
    },
);

TruckSchema.methods.setStatus = function(status) {
  this.status = status;
};

TruckSchema.methods.isUserTruck = function(user) {
  return String(this.created_by) === user._id;
};

TruckSchema.methods.isAssigned = function() {
  return Boolean(this.assigned_to);
};

const Truck = mongoose.model('Truck', TruckSchema);

module.exports = {Truck, statuses};
