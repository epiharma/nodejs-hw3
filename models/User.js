const mongoose = require('mongoose');
const jwt = require('jsonwebtoken');
const joi = require('joi');
const bcrypt = require('bcrypt');

const {JWT_SECRET} = require('../config/config');

const UserSchema = new mongoose.Schema({
  email: {
    type: String,
    required: true,
    unique: true,
  },
  role: {
    type: String,
    required: true,
  },
  password: {
    type: String,
    required: true,
  },
  createdDate: {
    type: Date,
    default: Date.now(),
  },
}, {
  collection: 'users',
},
);

UserSchema.virtual('loads', {
  ref: 'Load',
  localField: '_id',
  foreignField: 'created_by',
});

UserSchema.virtual('loads', {
  ref: 'Load',
  localField: '_id',
  foreignField: 'assigned_to',
});

UserSchema.pre('save', function(next) {
  if (!this.isModified('password')) {
    return next();
  }

  this.password = bcrypt.hashSync(this.password, 10);

  next();
});

UserSchema.methods.generateAuthToken = function() {
  return jwt.sign({
    _id: this._id,
    email: this.email,
    role: this.role,
    created_date: this.created_date,
  }, JWT_SECRET);
};

UserSchema.methods.checkPassword = function(receivedPassword) {
  return bcrypt.compareSync(receivedPassword, this.password);
};

const User = mongoose.model('User', UserSchema);

const roles = {
  SHIPPER: 'SHIPPER',
  DRIVER: 'DRIVER',
};

const validateUser = (user) => {
  const schema = joi.object({
    email: joi.string().empty().min(3).max(50).required()
        .messages({
          'string.base': `"email" should be a type of 'text'`,
          'string.empty': `"email" cannot be an empty field`,
          'string.min': `"email" should have a minimum length of {#limit}`,
          'string.max': `"email" should have a maximum length of {#limit}`,
          'any.required': `"email" is a required field`,
        }),
    password: joi.string().min(3).max(255).required()
        .messages({
          'string.base': `"password" should be a type of 'text'`,
          'string.empty': `"password" cannot be an empty field`,
          'string.min': `"password" should have a minimum length of {#limit}`,
          'string.max': `"password" should have a maximum length of {#limit}`,
          'any.required': `"password" is a required field`,
        }),
    role: joi.string().regex(new RegExp('^(SHIPPER)|(DRIVER)$')).required().
        messages({
          'string.base': `"role" should be a type of 'text'`,
          'string.empty': `"role" cannot be an empty field`,
          'string.pattern.base': 'SHIPPER or DRIVER is allowed',
          'any.required': `"role" is a required field`,
        })});

  return schema.validate(user);
};

module.exports = {User, validateUser, roles};
