const mongoose = require('mongoose');
const joi = require('joi');

const statuses = {
  NEW: 'NEW',
  POSTED: 'POSTED',
  ASSIGNED: 'ASSIGNED',
  SHIPPED: 'SHIPPED',
};

const states = {
  EN_ROUTE_TO_PICK_UP: 'En route to Pick Up',
  ARRIVED_TO_PICK_UP: 'Arrived to Pick Up',
  EN_ROUTE_TO_DELIVERY: 'En route to delivery',
  ARRIVED_TO_DELIVERY: 'Arrived to delivery',
};

const LoadSchema = new mongoose.Schema({
  created_by: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'users',
  },
  assigned_to: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'users',
  },
  status: {
    type: String,
    default: statuses.NEW,
  },
  state: {
    type: String,
    default: null,
  },
  name: {
    type: String,
    required: true,
  },
  payload: {
    type: Number,
    required: true,
  },
  pickup_address: {
    type: String,
    required: true,
  },
  delivery_address: {
    type: String,
    required: true,
  },
  dimensions: {
    width: {
      type: Number,
      required: true,
    },
    length: {
      type: Number,
      required: true,
    },
    height: {
      type: Number,
      required: true,
    },
  },
  logs: {
    type: Array,
    default: [],
  },
  created_date: {
    type: Date,
    default: Date.now(),
  },
}, {
  collection: 'loads',
  versionKey: false,
},
);


LoadSchema.methods.iterateToNextState = function() {
  switch (this.state) {
    case states.EN_ROUTE_TO_PICK_UP:
      this.state = states.ARRIVED_TO_PICK_UP;
      break;
    case states.ARRIVED_TO_PICK_UP:
      this.state = states.EN_ROUTE_TO_DELIVERY;
      break;
    case states.EN_ROUTE_TO_DELIVERY:
      this.state = states.ARRIVED_TO_DELIVERY;
      this.setStatus(statuses.SHIPPED);
      break;
    case states.ARRIVED_TO_DELIVERY:
      break;
    default:
      this.state = states.EN_ROUTE_TO_PICK_UP;
      break;
  }

  return this.state;
};

LoadSchema.methods.setStatus = function(statusName) {
  this.status = statusName;
};

LoadSchema.methods.assignTo = function(truck) {
  this.assigned_to = truck._id;
  this.logs.push({
    message: `Load assigned to driver with id ${truck.assigned_to}`,
    time: Date.now(),
  });
  this.setStatus(statuses.ASSIGNED);
  this.iterateToNextState();
};

LoadSchema.methods.isUserLoad = function(user) {
  return String(this.created_by) === String(user._id);
};

LoadSchema.methods.isInProgress = function() {
  return this.status !== statutes.NEW;
};


const Load = mongoose.model('Load', LoadSchema);

const validateLoad = (load) => {
  const schema = joi.object({
    name: joi.string().empty().min(3).max(50).required()
        .messages({
          'string.base': `"name" should be a type of 'text'`,
          'string.empty': `"name" cannot be an empty field`,
          'string.min': `"name" should have a minimum length of {#limit}`,
          'string.max': `"name" should have a maximum length of {#limit}`,
          'any.required': `"name" is a required field`,
        }),
    payload: joi.number().min(1).max(100500).required()
        .messages({
          'number.base': `"payload" should be a type of 'number'`,
          'number.min': `"payload" should have a minimum length of {#limit}`,
          'number.max': `"payload" should have a maximum length of {#limit}`,
          'any.required': `"payload" is a required field`,
        }),

    pickup_address: joi.string().min(1).max(255).required().messages({
      'string.base': `"pickup_address" should be a type of 'text'`,
      'string.empty': `"pickup_address" cannot be an empty field`,
      'string.min': `"pickup_address" should have a minimum length of {#limit}`,
      'string.max': `"pickup_address" should have a maximum length of {#limit}`,
      'any.required': `"pickup_address" is a required field`,
    }),
    delivery_address: joi.string().min(1).max(255).required().messages({
      'string.base': `"delivery_address" should be a type of 'text'`,
      'string.empty': `"delivery_address" cannot be an empty field`,
      'string.min': `"delivery_address" should have a minimum length of {#limit}`,
      'string.max': `"delivery_address" should have a maximum length of {#limit}`,
      'any.required': `"delivery_address" is a required field`,
    }),
    dimensions: joi.object({
      width: joi.number().min(1).max(100500).required(),
      length: joi.number().min(1).max(100500).required(),
      height: joi.number().min(1).max(100500).required(),
    }),
  });

  return schema.validate(load);
};

module.exports = {Load, validateLoad, statuses};
