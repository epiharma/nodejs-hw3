require('dotenv').config();

module.exports = {
  PORT: process.env.PORT || 8080,
  DB_USERNAME: process.env.DB_USERNAME || 'dbUser',
  DB_PASSWORD: process.env.DB_PASSWORD || 'DifficultPass!1',
  DB_NAME: process.env.DB_NAME || 'nodejs-hw2-mariia-ts',
  DB_CLUSTER: process.env.DB_CLUSTER || 'nodejshw.eq2dn',
  JWT_SECRET: process.env.JWT_SECRET || 'secret',
};
